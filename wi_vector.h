#ifndef __WI_VECTOR_H__
#define __WI_VECTOR_H__

#include <vector>
#include "wrapper.h"

class wi_vector : public std::vector<unsigned int>, public wrapper {
	unsigned int _idx;

public:
	wi_vector();
	virtual ~wi_vector();
	unsigned int& operator[](unsigned int);
	unsigned int size() const;
	void clear();
	void push_back(const unsigned int&);

	unsigned int get_last() const;
};

#endif /* __WI_VECTOR_H__ */
