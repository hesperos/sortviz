#include <iostream>

#include "app.h"
#include "gen_ex.h"

constexpr unsigned DEFAULT_MAX_ELEM = 160;
constexpr unsigned MAX_RAND_VALUE = 250;
constexpr unsigned SCREEN_X = 900;
constexpr unsigned SCREEN_Y = 400;


void help() {
	std::cout << "vis <algo> <dataset_size> <max_range> <data_type>" << std::endl;
	std::cout << "algo:" << std::endl;
	std::cout << "bubble, heap, insertion, merge, quicksort, quicksort2" << std::endl;
	std::cout << "radix, selection, shell, comb" << std::endl << std::endl;
	std::cout << "data_type:" << std::endl;
	std::cout << "inc,dec,random" << std::endl;
}


int main(int argc, char *argv[])
try
{
	unsigned n = DEFAULT_MAX_ELEM;
	unsigned r = MAX_RAND_VALUE;
	unsigned w = SCREEN_X;
	unsigned h = SCREEN_Y;

	std::string sorter = "";
	std::string gen = "";

	if (argc > 1) {
		if (argc < 5) {
			help();
			return -1;
		}

		sorter = argv[1];
		n = std::strtoul(argv[2], nullptr, 10);
		r = std::strtoul(argv[3], nullptr, 10);
		gen = argv[4];
	}

    app a;
    a.initialize(w, h);
    a.run(sorter, gen, n, r, argc == 1);
	return 0;
}
catch(const gen_ex& e) {
    std::cerr << "exception: " << e.what() << std::endl;
}
catch(...) {
    std::cerr << "unknown exception" << std::endl;
}
