#include "selection.h"
#include <unistd.h>
#include "factory.h"

static registrar<selection> r;

selection::selection(int d) :
	sorter(d) 
{
}

selection::~selection() 
{
}

void selection::operator()(wrapper& v) {
	int n = v.size() - 1;

	while (n) {
		int x = n;
		for (int i = 0; i < n; i++) {
			if (v[i] > v[x]) x = i;
		}

		if (x != n) {
			std::swap(v[n], v[x]);
		}
		n--;
	}
}

void selection::reg() {
	auto f = factory<sorter>::getInstance();
	f->reg("selection",
			[]() -> std::shared_ptr<sorter>  { return std::make_shared<selection>(selection(1)); } );
}
