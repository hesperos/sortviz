#ifndef RADIX_H_CSHBWTL7
#define RADIX_H_CSHBWTL7

#include "sort.h"

class radix : public sorter {
public:
	radix(int d = 50);
	virtual ~radix();
	void operator()(wrapper& v);
	static void reg();
};

#endif /* end of include guard: RADIX_H_CSHBWTL7 */

