#include "heap.h"
#include "factory.h"

#include <chrono>
#include <thread>

static registrar<heap> r;

unsigned int heap::_p(unsigned int i) {
	return i/2;
}

unsigned int heap::_l(unsigned int i) {
	return (2*i + 1);
}

unsigned int heap::_r(unsigned int i) {
	return (2*i + 2);
}

void heap::_heapify_down(wrapper& v, unsigned int s, unsigned int i) {
	unsigned int ex = i;
	unsigned int l = _l(i);
	unsigned int r = _r(i);

	if ( l <= s && v[l] > v[ex] ) {
		ex = l;
	}

	if ( r <= s && v[r] > v[ex] ) {
		ex = r;
	}

	if (ex != i) {
		std::swap(v[i], v[ex]);
		_heapify_down(v, s, ex);
		std::this_thread::sleep_for(std::chrono::microseconds(_delay/2));
	}
}

heap::heap(int d) :
	sorter(d) 
{
}

heap::~heap() 
{
}

void heap::operator()(wrapper& v) {
	int last = v.size() - 1;
	// build the heap
	for (int i = (last)/2; i>=0; i--) {
		_heapify_down(v, last, i);
		std::this_thread::sleep_for(std::chrono::microseconds(_delay));

	}

	// pop the values from the heap to the end of the array
	while (last) {
		const unsigned int tmp = v[0];
		v[0] = v[last];
		v[last] = tmp;
		last--;
		_heapify_down(v, last, 0);
		std::this_thread::sleep_for(std::chrono::microseconds(_delay));
	}
}

void heap::reg() {
	auto f = factory<sorter>::getInstance();
	f->reg("heap",
			[]() -> std::shared_ptr<sorter> { return std::make_shared<heap>(heap(40)); } );
}
