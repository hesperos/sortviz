#ifndef SORT_H_BKFS7ZIO
#define SORT_H_BKFS7ZIO

#include "wi_vector.h"
#include <memory>

class sorter {
protected:
	int _delay;

public:
	sorter(int delay = 400) : _delay(delay) {}
	virtual ~sorter() {}
	virtual void operator()(wrapper&) = 0;
};

#endif /* end of include guard: SORT_H_BKFS7ZIO */

