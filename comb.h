#ifndef COMB_H_VPXYCUFS
#define COMB_H_VPXYCUFS

#include "sort.h"

class comb : public sorter {
public:
	comb(int d = 300);
	virtual ~comb();
	void operator()(wrapper& v);
	static void reg(); 
};

#endif /* end of include guard: COMB_H_VPXYCUFS */

