#include "pigeonhole.h"
#include "factory.h"

#include <limits>
#include <iostream>
#include <chrono>
#include <thread>


static registrar<pigeonhole> r;

void pigeonhole::_find_extrems(wrapper& v) {
	_min = std::numeric_limits<unsigned int>::max();
	_max = std::numeric_limits<unsigned int>::min();

	for (int i = 0; i<v.size(); i++) {
		if (v[i] > _max) _max = v[i];
		if (v[i] < _min) _min = v[i];
	}
}

pigeonhole::pigeonhole(int d) :
	sorter(d),
	_min(0),
	_max(0)
{
}

pigeonhole::~pigeonhole() 
{
}

void pigeonhole::operator()(wrapper& v) {
	_find_extrems(v);
	_tmp.resize(_max - _min + 1);

	// zero the tmp
	for (int i = 0; i < _tmp.size(); i++) {
		_tmp[i] = 0;
	}
	
	// count the elements of given value
	for (int i = 0; i < v.size(); i++) {
		_tmp[ v[i] - _min ]++;
	}

	int z = 0;
	for (int i = _min; i <= _max; i++) {
		while (_tmp[i - _min]) {
			v[z++] = i;
			_tmp[i - _min]--;
			std::this_thread::sleep_for(std::chrono::microseconds(_delay));
		}
	}
}

void pigeonhole::reg() {
	auto f = factory<sorter>::getInstance();
	f->reg("pigeonhole",
			[]() -> std::shared_ptr<sorter> { return std::make_shared<pigeonhole>(pigeonhole(2000)); } );
}
