#include "bubble.h"
#include "factory.h"

#include <chrono>
#include <thread>

static registrar<bubble> r;

bubble::bubble(int d) :
	sorter(d) 
{
}


bubble::~bubble() 
{
}


void bubble::operator()(wrapper& v) {
	unsigned int n = v.size() - 1;
	bool swapped = true;

	while (n && swapped) {
		swapped = false;
		for (unsigned int i = 0; i < n; i++) {
			if (v[i] > v[i + 1]) {
				std::swap(v[i], v[i+1]);
				swapped = true;
			}

			if(_delay) 
				std::this_thread::sleep_for(std::chrono::microseconds(_delay));
		} // for
		n--;
	} // while
}


void bubble::reg() {
	auto f = factory<sorter>::getInstance();
	f->reg("bubble", 
			[]() -> std::shared_ptr<sorter> { return std::make_shared<bubble>(bubble(0)); });
}
