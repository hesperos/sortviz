#include "data_gen_random.h"
#include <ctime>
#include <cstdlib>

#include "factory.h"

static registrar<data_gen_random> r;

data_gen_random::data_gen_random() {
}


void data_gen_random::operator()(const std::shared_ptr<wrapper>& v,
		unsigned int n,
		unsigned int r) {
	v->clear();
	std::random_device rd;
	std::mt19937 rg(rd());
	std::uniform_int_distribution<unsigned> ud(0, r);

	for (int i = 0; i < n; i++) v->push_back(ud(rg));
	v->mv_set(r);
}


void data_gen_random::reg() {
	auto f = factory<data_gen>::getInstance();
	f->reg("random", 
			[]() -> std::shared_ptr<data_gen> { return std::make_shared<data_gen_random>(data_gen_random()); });
}

