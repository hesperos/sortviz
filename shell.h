#ifndef SHELL_H_PZAXKFGE
#define SHELL_H_PZAXKFGE

#include "sort.h"

class shell : public sorter {
public:
	shell(int d = 500);
	virtual ~shell();
	void operator()(wrapper& v);
	static void reg();
};

#endif /* end of include guard: SHELL_H_PZAXKFGE */

