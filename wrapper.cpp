#include "wrapper.h"
#include <algorithm>

void wrapper::add_vis(const std::shared_ptr<visualizer>& v) {
	_v.emplace_back(v);
}

void wrapper::del_vis(const std::shared_ptr<visualizer>& v) {
    auto it = std::find_if(_v.begin(), _v.end(), [&](const std::weak_ptr<visualizer>& p){
            if (auto locked = p.lock()) {
                return locked == v;
            }
            return false;
            });

	if (it != _v.end()) {
		_v.erase(it);
	}
}

void wrapper::notify() {
	std::vector<std::weak_ptr<visualizer> >::iterator it = _v.begin();
	while (it!=_v.end()) {
        if (auto locked = it->lock()) {
            locked->update();
        }
		++it;
	}
}

