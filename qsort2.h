#ifndef QSORT2_H_XUVHGFO6
#define QSORT2_H_XUVHGFO6

#include "sort.h"
#include <unistd.h>

class quicksort2 : public sorter {

	void _qs(wrapper& v, int p, int k);

public:
	quicksort2(int d = 100);
	virtual ~quicksort2();
	void operator()(wrapper& v);
	static void reg();
};



#endif /* end of include guard: QSORT2_H_XUVHGFO6 */
