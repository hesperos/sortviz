# algorithm visualizer

![App Screenshot]
(screenshot.png)

SDL application visualizing the work of popular algorithms.

Building (unix):
	cmake -G "Unix Makefiles"
	make

```
./vis <algo> <dataset_size> <max_range> <data_type>
algo:
bubble, heap, insertion, merge, quicksort, quicksort2
radix, selection, shell, comb

data_type:
inc,dec,random
```
