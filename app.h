#ifndef APP_H_DVYCYM82
#define APP_H_DVYCYM82

#include <memory>
#include <thread>

#include "wi_vector.h"
#include "vis_sdl.h"
#include "sort.h"
#include "data_gen.h"
#include "factory.h"


class app {
	std::shared_ptr<wi_vector> _x;
	std::shared_ptr<vis_sdl> _v;

	std::shared_ptr<factory<sorter> > _sf; 
	std::shared_ptr<factory<data_gen> > _gf; 

    bool _initialized;
    std::thread _exit_monitor;

	void _run_once(const std::shared_ptr<sorter>& sorter,
		const std::shared_ptr<data_gen>& gen,
		unsigned n,
		unsigned r);
public:
	app();
	void initialize(unsigned w, unsigned h);
	void run(const std::string&, const std::string& , unsigned, unsigned, bool);
	virtual ~app();
};

#endif /* end of include guard: APP_H_DVYCYM82 */
