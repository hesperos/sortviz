#ifndef DATA_GEN_RANDOM_H_JRGGRQHD
#define DATA_GEN_RANDOM_H_JRGGRQHD

#include "data_gen.h"
#include <random>

class data_gen_random : public data_gen {	
public:
	data_gen_random();
	virtual void operator()(const std::shared_ptr<wrapper>& v, 
			unsigned int n = 0, 
			unsigned int r = 0);

	static void reg();
};


#endif /* end of include guard: DATA_GEN_RANDOM_H_JRGGRQHD */

