#include "data_gen_dec.h"
#include <ctime>
#include <cstdlib>

#include "factory.h"

static registrar<data_gen_dec> r;

void data_gen_dec::operator()(const std::shared_ptr<wrapper>& v,
		unsigned int n,
		unsigned int r) {
	v->clear();
	double inc = (double)r/n;
	for (int i = n; i > 0; i--) v->push_back((inc*i) + 1);

	v->mv_set(r);
}

void data_gen_dec::reg() {
	auto f = factory<data_gen>::getInstance();
	f->reg("dec", 
			[]() -> std::shared_ptr<data_gen> { return std::make_shared<data_gen_dec>(data_gen_dec()); });
}

