#ifndef FACTORY_H_XQ6H3UEW
#define FACTORY_H_XQ6H3UEW

#include <map>
#include <functional>
#include <string>
#include <vector>
#include <memory>
#include <random>
#include <limits>

#include "sort.h"

template <typename T> using maker_t = std::function<std::shared_ptr<T> (void)>;

template<typename T = sorter> class factory {
	using reg_map_t = std::map<std::string, maker_t<T> >;
	using reg_map_it = typename reg_map_t::iterator;

	reg_map_t _ro;
	std::mt19937 _mr;
	std::uniform_int_distribution<unsigned> _ud;

protected:
	factory() :
		_ud(0, std::numeric_limits<unsigned>::max())
	{
		std::random_device rd;
		_mr.seed(rd());
	}

public:
	static std::shared_ptr<factory<T> > getInstance() {
		static std::shared_ptr<factory<T> > f(new factory<T>);
		return f;
	}


	std::shared_ptr<T> build(const std::string &s) {
		reg_map_it it;
		it	= _ro.find(s);
		
		if (it == _ro.end()) {
			return std::shared_ptr<T>();
		}

		return it->second();
	}

	
	std::shared_ptr<T> build_random(std::string &s) {
		reg_map_it it = _ro.begin();
		std::advance(it, (_ud(_mr) % _ro.size()));
		s = it->first;
		return build(it->first);
	}

	void reg(const std::string& s, maker_t<T> maker) {
		_ro[s] = maker;
	}
};


template<typename T> class registrar {
public:
	registrar() {
		T::reg();
	}
	virtual ~registrar() {}
};


#endif /* end of include guard: FACTORY_H_XQ6H3UEW */

