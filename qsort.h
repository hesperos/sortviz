#ifndef QSORT_H_EH7UOLBK
#define QSORT_H_EH7UOLBK


#include "sort.h"
#include <unistd.h>

class quicksort : public sorter {

	int _divide(wrapper& v, int p, int k);
	void _qs(wrapper& v, int p, int k);

public:
	quicksort(int d = 100);
	virtual ~quicksort();
	void operator()(wrapper& v);
	static void reg();
};



#endif /* end of include guard: QSORT_H_EH7UOLBK */

