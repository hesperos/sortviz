#include "vis_sdl.h"
#include <iostream>
#include <cmath>


vis_sdl::vis_sdl(const std::shared_ptr<wrapper>& v) :
	visualizer(v),
	_window(nullptr),
	_renderer(nullptr),
	_width(0),
	_height(0),
	_r(nullptr)
{
}


void vis_sdl::resize(const unsigned int& n) {
	if (_r) delete[] _r;
	_r = new SDL_Rect[n];
}


vis_sdl::~vis_sdl() {
	if (_r) delete[] _r;
}

void vis_sdl::_handle_events() {
	while (SDL_PollEvent(&_event) != 0) {
		if (_event.type == SDL_QUIT) {
			running = false;
			break;
		}
	}
}

void vis_sdl::init(unsigned int w, unsigned int h) {

	// call parent method
	// to register as an observer
	visualizer::init();

	// don't care about error now
	SDL_Init(SDL_INIT_VIDEO);
	_window = SDL_CreateWindow("Vis SDL", 
			SDL_WINDOWPOS_UNDEFINED,
			SDL_WINDOWPOS_UNDEFINED,
			w, h,
			SDL_WINDOW_SHOWN);

	_renderer = SDL_CreateRenderer(_window, -1, SDL_RENDERER_ACCELERATED);
	_set_color(0, 0xff, 0);
	_width = w;
	_height = h;
}


void vis_sdl::close() {
	SDL_DestroyRenderer(_renderer);
	SDL_DestroyWindow(_window);
	SDL_Quit();
}


void vis_sdl::_clear() {
	SDL_RenderClear(_renderer);
}


void vis_sdl::_draw_bar(int i, unsigned int x, unsigned int h, unsigned int t) {
	_r[i].x = x;
	_r[i].y = (int)(_height - h);
	_r[i].w = t;
	_r[i].h = h;
	SDL_RenderFillRect(_renderer, &_r[i]);
}


void vis_sdl::_set_color(unsigned int r, unsigned int g, unsigned int b) {
	SDL_SetRenderDrawColor(_renderer, r, g, b, 0xff);
}


void vis_sdl::set_title(const std::string& t) {
	_title = t;
	SDL_SetWindowTitle(_window, _title.c_str());
}


void vis_sdl::update() {
	auto v = std::dynamic_pointer_cast<wi_vector>(_w);
	int bw = ceil((float)_width) / v->size();
	int bw2 = 0;
	int mv = v->mv_get();
	int i = 0;
	wi_vector::iterator it = v->begin();

	if (bw == 1) {
		bw2 = 1;
	}
	else {
		bw2 = bw - 1;
	}

	_set_color(0,0,0);
	_clear();

	while (it != v->end()) {

		float hd = ((float)(*it)/ mv);
		int h = hd * (_height - 1);

		if (i == v->get_last()) {
			_set_color( 0xff, 0, 0 );
		}
		else {
			_set_color( 0x2f, 0xf0, 0);
		}

		_draw_bar(i, i * bw, h, bw2);

		i++;
		++it;
	}

	SDL_RenderPresent(_renderer);
	_handle_events();
}


