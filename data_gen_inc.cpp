#include "data_gen_inc.h"
#include <ctime>
#include <cstdlib>

#include "factory.h"

static registrar<data_gen_inc> r;

void data_gen_inc::operator()(const std::shared_ptr<wrapper>& v,
		unsigned int n,
		unsigned int r) {
	v->clear();
	double inc = (double)r/n;
	for (int i = 0; i < n; i++) v->push_back((inc*i) + 1);
	v->mv_set(r);
}

void data_gen_inc::reg() {
	auto f = factory<data_gen>::getInstance();
	f->reg("inc", 
			[]() -> std::shared_ptr<data_gen> { return std::make_shared<data_gen_inc>(data_gen_inc()); });
}

