#ifndef SELECTION_H_ICDXL2QM
#define SELECTION_H_ICDXL2QM

#include "sort.h"

class selection : public sorter {
public:
	selection(int d = 100);
	virtual ~selection();
	void operator()(wrapper& v);
	static void reg();
};


#endif /* end of include guard: SELECTION_H_ICDXL2QM */

