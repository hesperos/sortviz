#ifndef BUBBLE_H_C6XDY2IG
#define BUBBLE_H_C6XDY2IG

#include "sort.h"

class bubble : public sorter {
public:
	bubble(int d = 300);
	virtual ~bubble();
	void operator()(wrapper& v);
	static void reg(); 
};


#endif /* end of include guard: BUBBLE_H_C6XDY2IG */

