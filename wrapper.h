#ifndef WRAPPER_H_72QHXVPP
#define WRAPPER_H_72QHXVPP

#include <vector>
#include <memory>
#include "visualizer.h"

class wrapper {
	std::vector< std::weak_ptr<visualizer> > _v;

protected:
	unsigned int _max_value;

public:
	virtual ~wrapper() = default;

	virtual void add_vis(const std::shared_ptr<visualizer>&);
	virtual void del_vis(const std::shared_ptr<visualizer>&);
	virtual void notify();

	virtual unsigned int& operator[](unsigned int) = 0;
	virtual unsigned int size() const = 0;
	virtual void clear() = 0;
	virtual void push_back(const unsigned int&) = 0;

	unsigned int mv_get() const {
		return _max_value;
	}

	void mv_set(unsigned int mv) {
		_max_value = mv;
	}
};

#endif /* end of include guard: WRAPPER_H_72QHXVPP */

