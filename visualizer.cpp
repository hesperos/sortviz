#include "visualizer.h"
#include "wrapper.h"

visualizer::visualizer(const std::shared_ptr<wrapper>& w) : 
		_w(w),
		running(true)
{
}

void visualizer::init() {
	_w->add_vis(shared_from_this());
}
