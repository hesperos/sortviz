#include "insertion.h"
#include "factory.h"

#include <chrono>
#include <thread>

static registrar<insertion> r;

insertion::insertion(int d) :
	sorter(d) 
{
}

insertion::~insertion() 
{
}

void insertion::operator()(wrapper& v) {
	for (int i = 1; i < v.size(); i++) {
		int j = i;
		const unsigned int tmp = v[j];
		while (j > 0 && v[j-1] > tmp) {
			v[j] = v[j - 1];
			j--;
		}
		v[j] = tmp;
		std::this_thread::sleep_for(std::chrono::microseconds(_delay));
	}
}

void insertion::reg() {
	auto f = factory<sorter>::getInstance();
	f->reg("insertion",
			[]() -> std::shared_ptr<sorter>  { return std::make_shared<insertion>(insertion(10)); } );
}
