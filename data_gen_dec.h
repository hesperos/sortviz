#ifndef __DATA_GEN_DEC_H__
#define __DATA_GEN_DEC_H__

#include "data_gen.h"

class data_gen_dec : public data_gen {
public:
	virtual void operator()(const std::shared_ptr<wrapper>& v, 
			unsigned int n = 0, 
			unsigned int r = 0);
	static void reg();
};

#endif /* __DATA_GEN_DEC_H__ */
