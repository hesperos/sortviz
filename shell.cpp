#include "shell.h"
#include "factory.h"

#include <vector>
#include <chrono>
#include <thread>

static registrar<shell> r;

shell::shell(int d) :
	sorter(d) 
{
}

shell::~shell() 
{
}

void shell::operator()(wrapper& v) {
	std::vector<int> incs;

	// generate increments
	for (int h=1; h<v.size(); h = 3*h + 1) incs.insert(incs.begin(), h);

	for (int x = 0; x < incs.size(); x++) {
		for (int s = 0; s<incs[x]; s++) {				
			for (int i = incs[x] + s; i < v.size(); i += incs[x]) {
				int j = i;
				const unsigned int tmp = v[j];

				while (j > s && v[j - incs[x]] > tmp) {
					v[j] = v[j - incs[x]];
					j -= incs[x];
				}
				v[j] = tmp;
				std::this_thread::sleep_for(std::chrono::microseconds(_delay));
			}
		}
	}
}

void shell::reg() {
	auto f = factory<sorter>::getInstance();
	f->reg("shell",
			[]() -> std::shared_ptr<sorter>  { return std::make_shared<shell>(shell(100)); } );
}
