#include "gen_ex.h"


const char* gen_ex::errmsg_[] = {
    "Unable to Fabricate sorter",
    "Unable to Fabricate data generator",
    "Unknown Error",
};


gen_ex::gen_ex(const gen_err& err) :
    err_(err)
{
}

const char* gen_ex::what() const throw() {
    return errmsg_[err_];
}
