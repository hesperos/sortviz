#ifndef DATA_GEN_H_LOOP8RI7
#define DATA_GEN_H_LOOP8RI7


#include "wi_vector.h"
#include <memory>


class data_gen {
public:
	virtual void operator()(const std::shared_ptr<wrapper>& v, 
			unsigned int n = 0, 
			unsigned int r = 0) = 0;
};


#endif /* end of include guard: DATA_GEN_H_LOOP8RI7 */

