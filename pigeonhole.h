#ifndef __PIGEONHOLE_H__
#define __PIGEONHOLE_H__

#include "sort.h"

class pigeonhole : public sorter {
	unsigned int _min, _max;
	std::vector<unsigned int> _tmp;

	void _find_extrems(wrapper& v);

public:
	pigeonhole(int d = 100);
	virtual ~pigeonhole();
	void operator()(wrapper& v);
	static void reg();
};




#endif /* __PIGEONHOLE_H__ */
