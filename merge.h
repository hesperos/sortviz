#ifndef MERGE_H_EH7UOLBK
#define MERGE_H_EH7UOLBK

#include "sort.h"
#include <vector>


class mergesort : public sorter {
	std::vector<unsigned int> _tmp;

	void _merge(wrapper& v, int p, int m, int k);
	void _ms(wrapper& v, unsigned int p, unsigned int k);

public:
	mergesort(int d = 50);
	virtual ~mergesort();
	void operator()(wrapper& v);
	static void reg();
};



#endif /* end of include guard: MERGE_H_EH7UOLBK */

