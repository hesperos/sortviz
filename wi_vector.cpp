#include "wi_vector.h"
#include <iostream>

wi_vector::wi_vector() :
	_idx(0)
{
}

wi_vector::~wi_vector() 
{
}

unsigned int& wi_vector::operator[](unsigned int i) {
	_idx = i;
	notify();
	return std::vector<unsigned int>::operator[](i);
}


unsigned int wi_vector::size() const {
	return std::vector<unsigned int>::size();
}

void wi_vector::clear() {
	std::vector<unsigned int>::clear();
}


void wi_vector::push_back(const unsigned int& i) {
	std::vector<unsigned int>::push_back(i);
}

unsigned int wi_vector::get_last() const {
	return _idx;
}
