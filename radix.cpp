#include "radix.h"
#include "factory.h"

#include <vector>
#include <thread>
#include <chrono>

static registrar<radix> r;

radix::radix(int d) :
	sorter(d) 
{
}

radix::~radix() 
{
}

void radix::operator()(wrapper& v) {
	// queues for bits 0 and 1
	std::vector<unsigned int> _q[2];

	// go through the bits
	for (int i = 0; i<8; i++) {

		for (int j = 0; j<2; j++) _q[j].clear();

		// push to appropriate queue
		for (int j = 0; j<v.size(); j++) {
			_q[ (v[j] >> i) & 0x01 ].push_back(v[j]);
		}

		// doing it this way in order
		// for the observers to notice the changes
		int x = 0;
		for (int j = 0; j < 2; j++) {
			for (int i = 0; i<_q[j].size(); i++) {
				v[x++] = _q[j][i];
			}
		} // for

		std::this_thread::sleep_for(std::chrono::microseconds(_delay));
	}
}

void radix::reg() {
	auto f = factory<sorter>::getInstance();
	f->reg("radix",
			[]() -> std::shared_ptr<sorter>  { return std::make_shared<radix>(radix(100)); } );
}
