#include "qsort2.h"
#include "factory.h"

#include <chrono>
#include <thread>

static registrar<quicksort2> r;


void quicksort2::_qs(wrapper& v, int p, int k) {
	int low = p;
	int pv = 0;

	if (p>=k) return;

	pv = v[k];

	for (unsigned i = p; i<k; i++) {
		if (v[i] < pv) {
			if (i != low) std::swap(v[low], v[i]);
			low++;
		}
	}

	if (low != k) {
		std::swap(v[low], v[k]);
	}

	_qs(v, p, low - 1);
	_qs(v, low + 1, k);
}

quicksort2::quicksort2(int d) :
	sorter(d) 
{
}

quicksort2::~quicksort2() 
{
}

void quicksort2::operator()(wrapper& v) {
	_qs(v, 0, v.size() - 1);
}

void quicksort2::reg() {
	auto f = factory<sorter>::getInstance();
	f->reg("quicksort2",
			[]() -> std::shared_ptr<sorter>  { return std::make_shared<quicksort2>(quicksort2(50)); } );
}
