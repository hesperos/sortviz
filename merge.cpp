#include "merge.h"
#include "factory.h"

#include <chrono>
#include <thread>

static registrar<mergesort> r;

void mergesort::_merge(wrapper& v, int p, int m, int k) {
	int i1 = 0;
	int i2 = p;
	int i3 = m + 1;

	while ( i2<=m && i3<=k ) {
		if (v[i2] < v[i3]) {
			_tmp[i1++] = v[i2++];
		} 
		else {
			_tmp[i1++] = v[i3++];
		}
	}

	while (i2<=m) _tmp[i1++] = (v[i2++]);
	while (i3<=k) _tmp[i1++] = (v[i3++]);

	// copy back
	for (i2=p, i1=0;
			i2<=k; 
			i2++, i1++) {
		v[i2] = _tmp[i1];
		std::this_thread::sleep_for(std::chrono::microseconds(_delay));
	}
}

void mergesort::_ms(wrapper& v, unsigned int p, unsigned int k) {
	if (p < k) {
		int mid = (p + k)/2;
		_ms(v, p, mid);
		_ms(v, mid + 1, k);
		_merge(v, p, mid, k);
	}
}


mergesort::mergesort(int d) :
	sorter(d) 
{
}

mergesort::~mergesort() 
{
}

void mergesort::operator()(wrapper& v) {
	_tmp.resize(v.size());
	_ms(v, 0, v.size() - 1);
}

void mergesort::reg() {
	auto f = factory<sorter>::getInstance();
	f->reg("merge",
			[]() -> std::shared_ptr<sorter> { return std::make_shared<mergesort>(mergesort(10)); } );
}
