#include "comb.h"
#include <unistd.h>
#include "factory.h"

static registrar<comb> r;

comb::comb(int d) :
	sorter(d)
{
}


comb::~comb()
{
}


void comb::operator()(wrapper& v) {
	bool did_swap = true;
	int gap = v.size();

	while (gap > 1 || did_swap) {
		did_swap = false;

		gap = gap*10/13;
		if (gap <= 0) gap = 1;

		for (int i = 0; (i + gap) < v.size(); i++ ) {
			if (v[i] > v[i + gap]) {
				std::swap(v[i], v[i + gap]);
				did_swap = true;
			}
		}
	}
}

void comb::reg() {
	auto f = factory<sorter>::getInstance();
	f->reg("comb",
			[]() -> std::shared_ptr<sorter> { return std::make_shared<comb>(comb(100)); });
}
