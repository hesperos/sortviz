#ifndef HEAP_H_VEAMY2ZE
#define HEAP_H_VEAMY2ZE

#include "sort.h"

class heap : public sorter {
	unsigned int _p(unsigned int i) __attribute__((unused));
	unsigned int _l(unsigned int i) __attribute__((unused));
	unsigned int _r(unsigned int i) __attribute__((unused));
	void _heapify_down(wrapper& v, unsigned int s, unsigned int i);

public:
	heap(int d = 300);
	virtual ~heap();
	void operator()(wrapper& v);
	static void reg(); 
};


#endif /* end of include guard: HEAP_H_VEAMY2ZE */

