#include "app.h"
#include "gen_ex.h"

#include <cstdlib>


app::app() :
    _initialized(false)
{
	_x = std::make_shared<wi_vector>();
	_v = std::make_shared<vis_sdl>(_x);

	// get the factory instances
	_sf = factory<sorter>::getInstance();
	_gf = factory<data_gen>::getInstance();
}


app::~app() {
	if (_initialized && _v) _v->close();
}


void app::initialize(unsigned w, unsigned h) {
	_v->init(w, h);

    // this is such a dirty thing to do
    _exit_monitor = std::thread([&](){
            while (_v && _v->running) {
                std::this_thread::sleep_for(std::chrono::milliseconds(500));
            }
            std::exit(0);
     });

    _exit_monitor.detach();
    _initialized = true;
}


void app::_run_once(const std::shared_ptr<sorter>& sorter,
		const std::shared_ptr<data_gen>& gen,
		unsigned n,
		unsigned r) {

	// detach observer for the time of data generation
	_x->del_vis(_v);
	(*gen)(_x, n, r);
	_v->resize(n);
	_x->add_vis(_v);
	_v->update();
	(*sorter)(*_x);
}

#include <iostream>

void app::run(const std::string& a_sn,
		const std::string& a_gn,
		unsigned n,
		unsigned r,
		bool continous) {

	// names
	std::string sn = a_sn;
	std::string gn = a_gn;

	// should we do random selection ?
	bool random_s = !sn.length();
	bool random_g = !gn.length();

	do {
		// pointers
		std::shared_ptr<sorter> s;
		std::shared_ptr<data_gen> g;

		s = random_s ? _sf->build_random(sn) : _sf->build(sn);
		g = random_g ? _gf->build_random(gn) : _gf->build(gn);

		if (s.get() == nullptr)
            throw gen_ex(gen_ex::ErrSortCreat);

		if (g.get() == nullptr)
            throw gen_ex(gen_ex::ErrGenCreat);

		_v->set_title(sn + std::string(" ") + gn);
		_run_once(s, g, n, r);
	} while (continous && _v->running);
}



