#ifndef VIS_SDL_H_SNROCUFD
#define VIS_SDL_H_SNROCUFD

#include <SDL.h>
#include <string>
#include <memory>

#include "wi_vector.h"
#include "visualizer.h"

class vis_sdl : public visualizer {

	SDL_Window *_window;
	SDL_Renderer *_renderer;
	SDL_Rect *_r;
	SDL_Event _event;
	std::string _title;

	int _width;
	int _height;

	void _draw_bar(int i, unsigned int x, unsigned int h, unsigned int t);
	void _set_color(unsigned int r, unsigned int g, unsigned int b);
	void _clear();

	void _handle_events();

public:
	vis_sdl(const std::shared_ptr<wrapper>&);
	virtual ~vis_sdl();

	void init(unsigned int, unsigned int);
	void close();
	void update();
	void resize(const unsigned int&);
	void set_title(const std::string&);

};

#endif /* end of include guard: VIS_SDL_H_SNROCUFD */

