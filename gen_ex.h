#ifndef GEN_EX_H_E0NKEC6L
#define GEN_EX_H_E0NKEC6L

#include <exception>

class gen_ex : public std::exception {
public:
    enum gen_err {
        ErrSortCreat = 0,
        ErrGenCreat,

        ERR_LAST,
    };

    explicit gen_ex(const gen_err& err);
    virtual ~gen_ex() = default;

    virtual const char* what() const throw();

private:
    gen_err err_;

    static const char* errmsg_[];
};


#endif /* end of include guard: GEN_EX_H_E0NKEC6L */
