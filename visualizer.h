#ifndef VISUALIZER_H_VKJFCQEO
#define VISUALIZER_H_VKJFCQEO

#include <string>
#include <memory>

class wrapper;

class visualizer : 
	public std::enable_shared_from_this<visualizer> {

protected:
	std::shared_ptr<wrapper> _w;

public:
	bool running;

	visualizer(const std::shared_ptr<wrapper>& w);
	virtual	void init();
	virtual ~visualizer() {}

	virtual void update() = 0;
	virtual void resize(const unsigned int&) = 0;
	virtual void set_title(const std::string&) = 0;
};

#endif /* end of include guard: VISUALIZER_H_VKJFCQEO */

