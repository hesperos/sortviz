#ifndef INSERTION_H_EY8LYGLU
#define INSERTION_H_EY8LYGLU

#include "sort.h"
#include <unistd.h>

class insertion : public sorter {
public:
	insertion(int d = 300);
	virtual ~insertion();
	void operator()(wrapper& v);
	static void reg();
};



#endif /* end of include guard: INSERTION_H_EY8LYGLU */

